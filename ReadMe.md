## 最简单的基于FFmpeg的推流器

### 作者介绍
+ 雷霄骅 Lei Xiaohua leixiaohua1020@126.com
+ 中国传媒大学/数字电视技术 Communication University of China / Digital TV Technology
+ https://blog.csdn.net/leixiaohua1020/article/details/18155549

### 环境
Win 10 Visual Studio Community 2015


### 项目介绍
+ simplest_ffmpeg_streamer：推流器用于将本地文件以流媒体的形式发送出去。
+ simplest_ffmpeg_receiver：收流器用于将流媒体内容保存为本地文件。


#### 参考资料
+ [推流器](https://blog.csdn.net/leixiaohua1020/article/details/39803457)
+ [收流器](https://blog.csdn.net/leixiaohua1020/article/details/46890487)